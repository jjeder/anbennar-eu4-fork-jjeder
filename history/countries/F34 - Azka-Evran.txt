setup_vision = yes
government = monarchy
add_government_reform = phoenix_akalate
government_rank = 2
primary_culture = sun_elf
add_accepted_culture = bahari
religion = bulwari_sun_cult
technology_group = tech_bulwari
capital = 538
historical_friend = U19 #historical subject

1000.1.1 = { set_estate_privilege = estate_mages_organization_religious }

1442.1.1 = {	#his dad Kaltandir died in 1432
	monarch = {
		name = "Deggarion I"
		dynasty = "Evranzuir"
		birth_date = 1331.3.12
		adm = 2
		dip = 1
		mil = 5
	}
	add_ruler_personality = strict_personality
	add_ruler_personality = immortal_personality
	set_ruler_flag = set_immortality
}